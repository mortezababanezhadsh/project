<?php

namespace App\Policies;

use App\Demand;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DemandPolicy
{
    use HandlesAuthorization;

    public $user;
    public $demand;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(User $user, Demand $demand)
    {
        $this->user = $user;
    }

    public function manage(User $user)
    {
        return $user->ownsDemand() || $user->isAdmin();
    }
}
