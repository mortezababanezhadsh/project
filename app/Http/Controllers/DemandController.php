<?php

namespace App\Http\Controllers;

use App\Demand;
use App\Http\Requests\DemandRequest;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class DemandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('manage', auth()->user());
        $demands = Demand::with('states')->get();

        return view('admin.demands.index', compact('demands'));
    }


    public function allDemands()
    {
        $this->authorize('manage', auth()->user());
        $demands = Demand::all();
        $states = State::all();

        return view('admin.demands.all-demand', compact('demands', 'states'));
    }


    public function changeState(Request $request)
    {
        $demand = Demand::findOrFail($request->demand_id);

        if ($request->id) {
            $state = State::findOrFail($request->id);

            $demand->states()->sync([$state->id]);
        } elseif ($request->id == 0) {

            $demand->states()->detach();
        }

        return response()->json(['message' => 'وضعیت درخواست تغییر نمود']);
    }


    public function changeStateMultiple(Request $request)
    {
        // dd($request->all());

        $ids = $request->ids;
        $state = State::findOrFail($request->id);

        foreach ($ids as $id) {
            $demand = Demand::where('id', $id)->first();
            $demand->states()->sync([$state->id]);
        }

        return response()->json(['message' => 'وضعیت درخواست ها تغییر نمود']);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function create()
    {
        return view('admin.demands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DemandRequest $request)
    {
        // upload file
        if ($request->hasFile('file')) {
            $pathFile = $this->uploadFile($request->file('file'));
        }

        // sotre the demands
        $demand = auth()->user()->addDemand([
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price,
            'file' => $pathFile
        ]);


        return redirect(route('demands.index'))->with('message', 'درخواست شما با موفقیت ثبت شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Demand  $demand
     * @return \Illuminate\Http\Response
     */
    public function show(Demand $demand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Demand  $demand
     * @return \Illuminate\Http\Response
     */
    public function edit(Demand $demand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Demand  $demand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Demand $demand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Demand  $demand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Demand $demand)
    {
        $demand->delete();
        return redirect(route('demands.index'))->with('message', 'درخواست شما حذف شد');
    }

    public function uploadFile($file)
    {

        $fileName = $file->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'files',
            $file,
            $fileName
        );

        return $fileName;
    }

    public function download(Demand $demand)
    {

        return response()->download(storage_path('app/files/' . $demand->file));
    }
}
