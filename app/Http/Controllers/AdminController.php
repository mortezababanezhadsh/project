<?php

namespace App\Http\Controllers;

use App\Demand;
use App\State;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $this->authorize('manage', auth()->user());

        return view('admin.dashboard');
    }

    public function profile()
    {
        $user = auth()->user();
        return view('admin.profile.index', compact('user'));
    }
}
