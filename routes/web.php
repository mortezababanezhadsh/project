<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $user = auth()->user();
    if ($user->isAdmin()) {
        return view('admin.dashboard');
    }
    return view('admin.profile.index', compact('user'));
});

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::get('/profile', 'AdminController@profile')->name('profile');

    Route::get('/all-demand', 'DemandController@allDemands')->name('allDemands');
    Route::post('/demand/change-state', 'DemandController@changeState')->name('changeState');
    Route::post('/demand/change-state-multiple', 'DemandController@changeStateMultiple')->name('changeStateMultiple');

    Route::resource('demands', 'DemandController');
    Route::get('/demand/{demand}/download', 'DemandController@download')->name('download');

    Route::resource('states', 'StateController');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
