@extends('layouts.app')
@section('styles')
<style>
    html,
    body {
        background-color: #fff;
        color: #636b6f;
        height: 100vh;
        margin: 0;
        font-size: 40px;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 44px;
    }

    .links>a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        text-decoration: none;
    }

    .m-b-md {
        margin-bottom: 30px;
    }

</style>
@endsection
@section('content')
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
    <div class="top-right links">
        @auth
        <a href="{{ url('/') }}">داشبورد</a>
        @else
        <a href="{{ route('login') }}">ورود</a>

        @if (Route::has('register'))
        <a href="{{ route('register') }}">عضویت</a>
        @endif
        @endauth
    </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            سیستم درخواست پرداخت
        </div>

        <div class="links">
            <a href="/">داشبورد</a>
        </div>
    </div>
</div>
@endsection
