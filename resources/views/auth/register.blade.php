@extends('layouts.app')
@section('content')

        <div id="formWrapper">

            <div id="form" >
                <div class="logo">
                    <img src="https://www.vectorlogo.zone/logos/laravel/laravel-ar21.svg" alt="" srcset="">
                </div>
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                <div class="form-item">
                    <p class="formLabel">نام</p>
                    <input type="text" name="name" id="name" class="form-style @error('name') is-invalid @enderror" value="{{ old('name') }}" autocomplete="off"/>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>
                <div class="form-item">
                    <p class="formLabel">ایمیل</p>
                    <input type="email" name="email" id="email" class="form-style @error('email') is-invalid @enderror" value="{{ old('email') }}" autocomplete="off"/>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-item">
                    <p class="formLabel">رمز عبور</p>
                    <input type="password" name="password" id="password" class="form-style @error('password') is-invalid @enderror" />
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-item">
                    <p class="formLabel">تکرار رمز عبور</p>
                    <input type="password" name="password_confirmation" id="password-confirm" class="form-style" />
                </div>
                <div class="form-item">
                    <p class="pull-left"><a href="{{ route('login') }}"><small>ورود</small></a></p>
                    <input type="submit" class="login pull-right" value="عضویت">
                    <div class="clear-fix"></div>
                </div>
                </form>
            </div>
        </div>
@endsection
