@extends('layouts.app')
@section('content')

        <div id="formWrapper">

            <div id="form" >
                <div class="logo">
                    <img src="https://www.vectorlogo.zone/logos/laravel/laravel-ar21.svg" alt="" srcset="">
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                <div class="form-item">
                    <p class="formLabel">ایمیل</p>
                    <input type="email" name="email" id="email" class="form-style @error('email') is-invalid @enderror" autocomplete="off"/>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
                    @enderror
                </div>
                <div class="form-item">
                    <p class="formLabel">رمز عبور</p>
                    <input type="password" name="password" id="password" class="form-style @error('password') is-invalid @enderror" />
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
                    @enderror
                <!-- <div class="pw-view"><i class="fa fa-eye"></i></div> -->
                    @if (Route::has('password.request'))
                        <p><a href="{{ route('password.request') }}" ><small>فراموشی رمز ؟</small></a></p>
                    @endif
                </div>
                <div class="form-item">
                    <p class="pull-left"><a href="{{ route('register') }}"><small>عضویت</small></a></p>
                    <input type="submit" class="login pull-right" value="ورود">
                    <div class="clear-fix"></div>
                </div>
                </form>
            </div>
        </div>


@endsection
