<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Start layout styles -->

    <link rel="stylesheet" href="http://cdn.materialdesignicons.com/5.1.45/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}" />
    <!-- End layout styles -->
    <title>داشبورد</title>
</head>

<body class="rtl">
    <!-- Miscellanoeous  Miscellaneous-->
    <div class="container-scroller">
        <!-- navbar starts here -->
        @include('admin.layout.nav')
        <!-- ends here -->
        <div class="container-fluid page-body-wrapper">
            <!-- settings-panel starts here -->
            <!-- sidebar starts here -->
            @include('admin.layout.sidebar')
            <!-- ends here -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        @yield('content')
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- footer starts here-->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">سیستم مدیریت © 2020
                        </span>
                        {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">ساخته شده توسط <i
                                class="mdi mdi-heart text-danger"></i></span> --}}
                    </div>
                </footer>
                <!-- ends here -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- Jquery & Popper & Bootstrap:js -->
    <script src="{{asset('js/app.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable();
        });

    </script>
    @yield('scripts')
    <!-- endinject -->
</body>

</html>
