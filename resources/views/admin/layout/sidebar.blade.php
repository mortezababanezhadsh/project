<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
                <div class="nav-profile-image">
                    <img src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
                        alt="profile" />
                    <span class="login-status online"></span>
                    <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column">
                    <span class="font-weight-bold mb-2">مرتضی بابانژاد</span>
                    <span class="text-secondary text-small">مدير سایت</span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
        </li>
        @can('manage', auth()->user())
        <li class="nav-item">
            <a class="nav-link" href="{{route('dashboard')}}">
                <span class="menu-title">داشبورد</span>
                <i class="mdi mdi-home menu-icon"></i>
            </a>
        </li>
        @endcan
        <li class="nav-item">
            <a class="nav-link" href="{{route('profile')}}">
                <span class="menu-title">پروفایل</span>
                <i class="mdi menu-icon">
                    <svg class="bi bi-person-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                    </svg>
                </i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('demands.create')}}">
                <span class="menu-title">ثبت درخواست</span>
                <i class="mdi menu-icon">
                    <svg class="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                        <path fill-rule="evenodd"
                            d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                    </svg>
                </i>
            </a>
        </li>
        @can('manage', auth()->user())
        <li class="nav-item">
            <a class="nav-link" href="{{route('allDemands')}}">
                <span class="menu-title">درخواست ها</span>
                <i class="mdi menu-icon">
                    <svg class="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                        <path fill-rule="evenodd"
                            d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                    </svg>
                </i>
            </a>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="{{route('states.index')}}">
                <span class="menu-title">وضعیت ها</span>
                <i class="mdi menu-icon">
                    <svg class="bi bi-list-check" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z" />
                    </svg>
                </i>
            </a>
        </li>
        @endcan
        <!-- <li class="nav-item">
          <a
            class="nav-link"
            data-toggle="collapse"
            href="#apps"
            aria-expanded="false"
            aria-controls="apps"
          >
            <span class="menu-title">فروشگاه</span>
            <i class="menu-arrow"></i>
            <i class="mdi mdi-cart-arrow-down menu-icon"></i>
          </a>
          <div class="collapse" id="apps">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item">
                <a
                  class="nav-link"
                  href="../../pages/apps/kanban-board.html"
                  >همه محصولات</a
                >
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../../pages/apps/todo.html"
                  >ثبت محصول جدید</a
                >
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../../pages/apps/tickets.html"
                  >روش ارسال کالا</a
                >
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../../pages/apps/chats.html"
                  >کوپن تخفیف</a
                >
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../../pages/samples/widgets.html">
            <span class="menu-title">تنظیمات</span>
            <i class="mdi mdi-forum menu-icon"></i>
          </a>
        </li> -->
    </ul>
</nav>
