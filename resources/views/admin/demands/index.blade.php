@extends('admin.layout.master')

@section('content')

<div class="col-md-12 grid-margin stretch-card">
    @include('includes.flash')
    <div class="card py-4">
        <div class="card-body">
            <div class="mb-4">
                <a href="{{ route('demands.create') }}" class="btn btn-success btn-sm">ثبت درخواست جدید</a>
            </div>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">عنوان</th>
                        <th scope="col">توضیحات</th>
                        <th scope="col">مبلغ</th>
                        <th scope="col">فایل</th>
                        <th scope="col">وضعیت</th>
                        {{-- <th scope="col">تنظیمات</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @if($demands)
                    @foreach($demands as $demand)
                    <tr>
                        <td>{{$demand->title}}</td>
                        <td>{{$demand->description}}</td>
                        <td>{{$demand->price}} ریال</td>
                        <td><a href="{{ route('download', $demand->id) }}">{{$demand->file }}</a></td>
                        <td>
                            @if (count($demand->states))
                            <span class="text-info">{{$demand->states[0]->name }}</span>
                            @else
                            <span class="text-warning">بدون وضعیت</span>
                            @endif
                        </td>
                        {{-- <td>
              <form action="{{route('demands.destroy', $demand->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <div class="d-flex justify-bitween-content">
                            <button type="submit" class="btn btn-sm btn-danger mx-1">
                                <svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16"
                                    fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                    <path fill-rule="evenodd"
                                        d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                </svg>
                            </button>
                            <a href="{{route('demands.edit', $demand->id)}}" class="btn btn-sm btn-primary">
                                <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16"
                                    fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                    <path fill-rule="evenodd"
                                        d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                </svg>
                            </a>
                        </div>
                        </form>
                        </td> --}}
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
