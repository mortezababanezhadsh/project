@extends('admin.layout.master')
@section('content')

<div class="col-md-12 grid-margin stretch-card">
    @include('includes.errors')
    <div class="card my-4">
        <div class="card-body">
            <form method="POST" action="{{route('demands.store')}}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="title">عنوان</label>
                    <input type="text" class="form-control" value="{{$demand->title}}" id="title" name="title"
                        placeholder="عنوان را وارد کنید..">
                </div>
                <div class="form-group">
                    <label for="description">توضیحات</label>
                    <textarea rows="5" type="text" class="form-control" id="description" name="description"
                        placeholder="توضیحات را وارد کنید..">{{$demand->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="price">مبلغ</label>
                    <input type="text" class="form-control" id="price" value="{{$demand->price}}" name="price"
                        placeholder="مبلغ را وارد کنید..">
                </div>
                <div class="form-group">
                    <label for="description">فایل</label>
                    <input type="file" class="form-control" id="file" name="file" placeholder="توضیحات را وارد کنید..">
                </div>
                <button type="submit" class="btn btn-primary">ثبت درخواست</button>
            </form>
        </div>
    </div>
</div>
@endsection