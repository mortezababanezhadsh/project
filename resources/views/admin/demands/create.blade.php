@extends('admin.layout.master')
@section('content')

<div class="col-md-12 grid-margin stretch-card">
    @include('includes.errors')
    <div class="card my-4">
        <div class="card-body">
            <form method="POST" action="{{route('demands.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">عنوان</label>
                    <input type="text" class="form-control" value="{{old('title')}}" id="title" name="title"
                        placeholder="عنوان را وارد کنید..">
                </div>
                <div class="form-group">
                    <label for="description">توضیحات</label>
                    <textarea rows="5" type="text" class="form-control" id="description" name="description"
                        placeholder="توضیحات را وارد کنید..">{{old('description')}}</textarea>
                </div>
                <div class="form-group">
                    <label for="price">مبلغ</label>
                    <input type="text" class="form-control" id="price" value="{{old('price')}}" name="price"
                        placeholder="مبلغ را به ریال وارد کنید..">
                </div>
                <div class="form-group">
                    <label for="description">فایل</label>
                    <input type="file" class="form-control" id="file" name="file" placeholder="توضیحات را وارد کنید..">
                </div>
                <button type="submit" class="btn btn-primary my-2">ثبت درخواست</button>
                <a href="{{ route('demands.index') }}" class="btn btn-secondary">کنسل</a>
            </form>
        </div>
    </div>
</div>
@endsection