@extends('admin.layout.master')
@section('content')
<div class="col-md-12 grid-margin stretch-card">
<div class="card my-4">
   <div class="card-body">
    <form method="POST" action="{{route('states.update', $state->id)}}">
        @csrf
        @method('PATCH')
        @include('includes.errors')
        @include('includes.flash')
        <div class="form-group">
            <label for="name">عنوان</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$state->name}}" placeholder="نام را وارد کنید..">
        </div>
        <button type="submit" class="btn btn-primary">ویرایش وضعیت</button>
    </form>
  </div>
</div>
</div>
@endsection
