@extends('admin.layout.master')
@section('content')

<div class="col-md-12 grid-margin stretch-card">
    @include('includes.flash')
    <div class="card py-4">
        <div class="card-body">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-cube text-danger icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-1 text-right">کل پرداختی</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-1">
                                            2,265,000 تومان
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-1">
                                <i class="mdi mdi-arrow-left-bold-circle-outline mr-1" aria-hidden="true"></i> مشاهده
                                تمام پرداختی ها
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-account-box-multiple text-info icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-1 text-right">کارکنان</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-1">254 نفر</h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-1">
                                <i class="mdi mdi-arrow-left-bold-circle-outline mr-1" aria-hidden="true"></i> مشاهده
                                اطلاعات کارکنان </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-poll-box text-success icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-1 text-right">درخواست پرداخت جدید</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-1">25</h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-1">
                                <a href="{{ route('allDemands') }}">
                                    <i class="mdi mdi-arrow-left-bold-circle-outline mr-1" aria-hidden="true"></i>
                                    مشاهده
                                    درخواست های جدید
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
