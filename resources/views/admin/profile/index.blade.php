@extends('admin.layout.master')

@section('content')

<div class="col-md-12 grid-margin stretch-card">
    @include('includes.flash')
    <div class="card py-4">
        <div class="card-body">
            <div class="card mb-3">
                <div class="card-header d-flex justify-content-between p-2">
                    <h5 class="">اطلاعات شخصی</h5>
                    <div>
                        <a href="{{ route('demands.index') }}" class="btn btn-success btn-sm">مشاهده درخواست ها</a>
                        <a href="{{ route('demands.create') }}" class="btn btn-success btn-sm">ثبت درخواست جدید</a>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <span>نام کاربری: {{$user->name}}</span>
                    </div>
                    <div>
                        <span>ایمیل کاربری: {{$user->email}}</span>
                    </div>
                    <div>
                        <span>کد کاربری: {{$user->code}}</span>
                    </div>
                </div>
            </div>

            <div class="card">
                <h5 class="card-header">اطلاعات بانکی</h5>

                <div class="card-body">
                    <div>
                        <span>نام بانک: بانک ملی</span>
                    </div>
                    <div>
                        <span>شماره حساب: 1111-1111-1111-1111</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
